

function validationBlock(block){
    let input = block.querySelector('.input');
    let error, notifyBlock;
    let inputControl = input.parentElement;
    // let notifyBlock = block.querySelector('.notify-block');
    let el;
    let close = document.createElement('i');
    close.classList.add('fa');
    close.classList.add('fa-times-circle');
    close.classList.add('remove-notify');

    close.addEventListener('click', removeNotify);

    // console.log(input);

    input.addEventListener('blur', function(event){
        el = event.target;
        checkValue();
    });

    function checkValue() {
        error = block.querySelector('.error');
        notifyBlock = block.querySelector('.notify-block');
        console.log(notifyBlock);
        if(valid()){
            setNotify();
        }else{
            setErrorMsg()
        }
    }
    function valid() {
        let isValid = true;
        let type = 'string';
        if(el.classList.contains('price'))
            type = 'price';

        switch (type) {
            case 'price': {
                if(isNaN(parseInt(el.value)) || parseInt(el.value) < 0 ) isValid = false;
                break;
            }
        }
        return isValid;
    }

    function setErrorMsg() {
        if(!error) {
            error = document.createElement('p');
            error.classList.add('error');
            error.textContent = 'Please enter correct price';
        }
        inputControl.append(error);

        if(notifyBlock){
            removeNotifyBlock();
        }
    }
    
    function setNotify() {
        if(error) error.remove();

        if(!el.classList.contains('notify')) return; // add notify if it needs

        if(notifyBlock){
            notifyBlock.textContent = `Текущая цена: ${el.value}`;
        }
        else{
            notifyBlock = document.createElement('span');
            notifyBlock.textContent = `Текущая цена: ${el.value}`;
            notifyBlock.classList.add('notify-block');
            // let close = document.createElement('i');
            // close.classList.add('fa');
            // close.classList.add('fa-times-circle');
            // close.classList.add('remove-notify');
            //
            // close.addEventListener('click', removeNotify);

            // notifyBlock.append(close);

            let inputControl = el.parentElement;
            inputControl.prepend(notifyBlock);

        }
        notifyBlock.append(close);

    }

    function removeNotify(event) {
        event.target.parentElement.remove();
        event.target.removeEventListener('click', removeNotify)
    }
    function removeNotifyBlock() {
        notifyBlock.remove();
        notifyBlock.querySelector('.remove-notify').removeEventListener('click', removeNotify)
    }
}


document.querySelector('form').addEventListener('submit', function (e) {
    e.preventDefault();
})
// let block = document.querySelector('.validation-block');
document.querySelectorAll('.validation-block').forEach(block=>{
    validationBlock(block);
});
